/*
 * Copyright (c) 2017 Oleg Filimonov
 */

package ru.olegfilimonov.apiery;

import android.support.multidex.MultiDexApplication;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.List;
import ru.olegfilimonov.apiery.model.Apiery;
import ru.olegfilimonov.apiery.model.Constant;
import ru.olegfilimonov.apiery.model.Data;
import ru.olegfilimonov.apiery.model.Event;
import ru.olegfilimonov.apiery.model.Hive;
import ru.olegfilimonov.apiery.model.Tag;

/**
 * @author Oleg Filimonov
 */

public class ApieryApplication extends MultiDexApplication {
  private static ApieryApplication instance;
  private List<Apiery> apieries;
  private List<Event> events;
  private List<Tag> tags;

  public ApieryApplication() {
    instance = this;
  }

  public static ApieryApplication getInstance() {
    return instance;
  }

  public void loadDataFromServer(final ValueEventListener listener) {

    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

    if (user == null) return;

    // Write a message to the database
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference(user.getUid());
    myRef.addListenerForSingleValueEvent(new ValueEventListener() {
      @Override public void onDataChange(DataSnapshot dataSnapshot) {

        Data data = dataSnapshot.getValue(Data.class);

        if (data != null) {
          apieries = data.getApieries();
          events = data.getEvents();
          tags = data.getStatuses();
        }

        listener.onDataChange(dataSnapshot);
      }

      @Override public void onCancelled(DatabaseError databaseError) {
        listener.onCancelled(databaseError);
      }
    });
  }

  public void updateDataOnServer() {

    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

    if (user == null) return;

    Data data = new Data(events, apieries, tags);

    // Write a message to the database
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference(user.getUid());

    myRef.setValue(data);
  }

  public List<Tag> getTags() {
    if (tags == null) {
      tags = new ArrayList<>();
    }
    return tags;
  }

  public Tag getStatus(long stepId) {
    if (stepId == -1) return null;
    if (tags == null) {
      tags = new ArrayList<>();
    }
    for (Tag tag : tags) {
      if (tag.getId() == stepId) return tag;
    }
    return null;
  }

  public List<Apiery> getApieries() {
    if (apieries == null) {
      apieries = new ArrayList<>();
    }
    return apieries;
  }

  public List<Event> getEvents() {
    if (events == null) {
      events = new ArrayList<>();
    }
    return events;
  }

  public List<Event> getEventsByHiveId(long id) {
    List<Event> events = getEvents();
    List<Event> result = new ArrayList<>();
    for (Event event : events) {
      if (event.getHiveId() != null && event.getHiveId() == id) {
        result.add(event);
      }
    }

    return result;
  }

  public List<Hive> getAllHives() {
    List<Hive> hives = new ArrayList<>();

    for (Apiery apiery : apieries) {
      hives.addAll(apiery.getHives());
    }

    return hives;
  }

  public void addEvent(Event event) {
    if (event.getId() == null) {
      event.setId(getNextUnusedEventId());
    }
    events.add(event);
    updateDataOnServer();
  }

  public void addHive(Hive hive) {
    Apiery apiery = getApieryById(hive.getApieryId());
    if (apiery == null) {
      ArrayList<Hive> hives = new ArrayList<>();
      hives.add(hive);
      apiery = new Apiery(hive.getApieryId(), hive.getLocation(), hives, "Точок " + hive.getApieryId());
      apieries.add(apiery);
    } else {
      apiery.getHives().add(hive);
    }
    updateDataOnServer();
  }

  public Apiery getApieryById(long apieryId) {
    for (Apiery apiery : apieries) {
      if (apiery.getId() == apieryId) return apiery;
    }
    return null;
  }

  public long getNextUnusedHiveId() {
    for (int i = 0; i < Constant.MAX_AMOUNT_OF_ITEMS; i++) {
      boolean taken = false;
      for (Hive hive : getAllHives()) {
        if (hive.getId() == i) {
          taken = true;
          break;
        }
      }
      if (!taken) return i;
    }
    return -1;
  }

  public long getNextUnusedApieryId() {
    for (int i = 0; i < Constant.MAX_AMOUNT_OF_ITEMS; i++) {
      boolean taken = false;
      for (Apiery apiery : apieries) {
        if (apiery.getId() == i) {
          taken = true;
          break;
        }
      }
      if (!taken) return i;
    }
    return -1;
  }

  public long getNextUnusedEventId() {
    for (int i = 0; i < Constant.MAX_AMOUNT_OF_ITEMS; i++) {
      boolean taken = false;
      for (Event event : events) {
        if (event.getId() == i) {
          taken = true;
          break;
        }
      }
      if (!taken) return i;
    }
    return -1;
  }

  public Hive getHiveById(Long hiveId) {
    for (Hive hive : getAllHives()) {
      if (hive.getId() == hiveId) return hive;
    }
    return null;
  }

  public void removeEvent(Event event) {
    events.remove(event);
    updateDataOnServer();
  }
}
