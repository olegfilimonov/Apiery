/*
 * Copyright (c) 2017 Oleg Filimonov
 */

package ru.olegfilimonov.apiery;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import ru.olegfilimonov.apiery.activity.HiveActivity;
import ru.olegfilimonov.apiery.activity.MainActivity;

public class AlarmReceiver extends BroadcastReceiver {
  @Override public void onReceive(Context context, Intent intent) {
    Log.d("lol", "onReceive: hi");

    Intent notificationIntent = null;

    if (intent.hasExtra("hiveId")) {
      // Has a hive
      notificationIntent = new Intent(context, HiveActivity.class);
      notificationIntent.putExtra("hive_id", Long.parseLong(intent.getStringExtra("hiveId")));
    } else {
      notificationIntent = new Intent(context, MainActivity.class);
    }

    String title = intent.getStringExtra("title");
    String message = intent.getStringExtra("message");

    sendNotification(title, message, notificationIntent, context);
  }

  /**
   * Create and show a simple notification containing the received FCM message.
   */
  private void sendNotification(String title, String message, Intent intent, Context context) {
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);

    Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context).setSmallIcon(R.drawable.ic_apiery)
        .setContentTitle(title).setColor(context.getResources().getColor(R.color.colorPrimary))
        .setContentText(message)
        .setAutoCancel(true)
        .setSound(defaultSoundUri)
        .setContentIntent(pendingIntent);

    NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

    notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
  }
}