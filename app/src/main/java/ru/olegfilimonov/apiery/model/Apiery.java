/*
 * Copyright (c) 2017 Oleg Filimonov
 */

package ru.olegfilimonov.apiery.model;

import java.util.List;

/**
 * @author Oleg Filimonov
 */

public class Apiery {
  private LatLng location;
  private long id;
  private List<Hive> hives;
  private String title;
  private boolean expanded;

  public Apiery() {
  }

  public Apiery(long id, LatLng location, List<Hive> hives, String title) {
    this.location = location;
    this.id = id;
    setHives(hives);
    this.title = title;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public LatLng getLocation() {
    return location;
  }

  public void setLocation(LatLng location) {
    this.location = location;
  }

  public List<Hive> getHives() {
    return hives;
  }

  public void setHives(List<Hive> hives) {
    for (Hive hive : hives) {
      hive.setApieryId(id);
    }
    this.hives = hives;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public boolean isExpanded() {
    return expanded;
  }

  public void setExpanded(boolean expanded) {
    this.expanded = expanded;
  }
}
