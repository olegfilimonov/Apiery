/*
 * Copyright (c) 2017 Oleg Filimonov
 */

package ru.olegfilimonov.apiery.model;

/**
 * @author Oleg Filimonov
 */

public class Constant {
  public static final boolean DEFAULT_DISTANCE_ENABLED = false;
  public static final int DEFAULT_DISTANCE = 30; //m
  public static final int DEFAULT_MAX_DISTANCE = 400; //m
  public static final int MAX_AMOUNT_OF_ITEMS = 100000;
  public static float APIERIES_ZOOM_LIMIT = 18;
}
