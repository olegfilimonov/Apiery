/*
 * Copyright (c) 2017 Oleg Filimonov
 */

package ru.olegfilimonov.apiery.model;

/**
 * @author Oleg Filimonov
 */

public enum MapState {
  APIERIES, HIVES
}
