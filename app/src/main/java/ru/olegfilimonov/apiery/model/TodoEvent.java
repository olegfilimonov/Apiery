/*
 * Copyright (c) 2017 Oleg Filimonov
 */

package ru.olegfilimonov.apiery.model;

/**
 * @author Oleg Filimonov
 */

public class TodoEvent extends Event {

  public TodoEvent() {
  }

  public Integer getIconRes() {
    return -1;
  }

  public Integer getIconTint() {
    return null;
  }
}
