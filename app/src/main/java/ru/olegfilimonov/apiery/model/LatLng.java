/*
 * Copyright (c) 2017 Oleg Filimonov
 */

package ru.olegfilimonov.apiery.model;

/**
 * @author Oleg Filimonov
 */

public class LatLng {
  public double latitude;
  public double longitude;

  public LatLng() {
  }

  public LatLng(double latitude, double longitude) {
    this.latitude = latitude;
    this.longitude = longitude;
  }

  public LatLng(com.google.android.gms.maps.model.LatLng latLng) {
    this(latLng.latitude, latLng.longitude);
  }

  public double getLatitude() {
    return latitude;
  }

  public void setLatitude(double latitude) {
    this.latitude = latitude;
  }

  public double getLongitude() {
    return longitude;
  }

  public void setLongitude(double longitude) {
    this.longitude = longitude;
  }
}
