/*
 * Copyright (c) 2017 Oleg Filimonov
 */

package ru.olegfilimonov.apiery.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author Oleg Filimonov
 */

public class Hive implements Parcelable {
  public static final Creator<Hive> CREATOR = new Creator<Hive>() {
    @Override public Hive createFromParcel(Parcel in) {
      return new Hive(in);
    }

    @Override public Hive[] newArray(int size) {
      return new Hive[size];
    }
  };
  private long id;
  private long apieryId;
  private long tagsId = -1;
  private LatLng location;
  private String name;
  private String note;
  private long createdDateTime;

  public Hive() {
  }

  protected Hive(Parcel in) {
    id = in.readLong();
    apieryId = in.readLong();
    name = in.readString();
    note = in.readString();
    location = new LatLng(in.readDouble(), in.readDouble());
    tagsId = in.readLong();
  }

  public void update(Hive newHive) {
    name = newHive.getName();
    location = newHive.getLocation();
    note = newHive.getNote();
    tagsId = newHive.getTagsId();
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeLong(id);
    dest.writeLong(apieryId);
    dest.writeString(name);
    dest.writeString(note);
    dest.writeDouble(location.latitude);
    dest.writeDouble(location.longitude);
    dest.writeLong(tagsId);
  }

  @Override public int describeContents() {
    return 0;
  }

  public LatLng getLocation() {
    return location;
  }

  public void setLocation(LatLng location) {
    this.location = location;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public long getApieryId() {
    return apieryId;
  }

  public void setApieryId(long apieryId) {
    this.apieryId = apieryId;
  }

  public long getCreatedDateTime() {
    return createdDateTime;
  }

  public void setCreatedDateTime(long createdDateTime) {
    this.createdDateTime = createdDateTime;
  }

  public long getTagsId() {
    return tagsId;
  }

  public void setTagsId(long tagsId) {
    this.tagsId = tagsId;
  }
}
