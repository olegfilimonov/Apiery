/*
 * Copyright (c) 2017 Oleg Filimonov
 */

package ru.olegfilimonov.apiery.model;

/**
 * @author Oleg Filimonov
 */

public class Tag {
  private long id;
  private String text;

  public Tag() {
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }
}
