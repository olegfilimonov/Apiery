/*
 * Copyright (c) 2017 Oleg Filimonov
 */

package ru.olegfilimonov.apiery.model;

import android.content.Context;
import android.support.annotation.Nullable;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Months;
import org.joda.time.Seconds;
import org.joda.time.Weeks;
import org.joda.time.Years;
import ru.olegfilimonov.apiery.R;

/**
 * @author Oleg Filimonov
 */

public class Event {
  public long id;
  protected String text;
  private String title;
  @Nullable private Long hiveId;
  private Long shownDateTime;

  public Event() {
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  @Nullable public Long getHiveId() {
    return hiveId;
  }

  public void setHiveId(@Nullable Long hiveId) {
    this.hiveId = hiveId;
  }

  public long getShownDateTime() {
    return shownDateTime;
  }

  public void setShownDateTime(long shownDateTime) {
    this.shownDateTime = shownDateTime;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getElapsedTime(Context context) {
    DateTime current = new DateTime(System.currentTimeMillis());
    int seconds = Math.abs(Seconds.secondsBetween(current, new DateTime(shownDateTime)).getSeconds());
    int minutes = Math.abs(Minutes.minutesBetween(current, new DateTime(shownDateTime)).getMinutes());
    int hours = Math.abs(Hours.hoursBetween(current, new DateTime(shownDateTime)).getHours());
    int days = Math.abs(Days.daysBetween(current, new DateTime(shownDateTime)).getDays());
    int weeks = Math.abs(Weeks.weeksBetween(current, new DateTime(shownDateTime)).getWeeks());
    int months = Math.abs(Months.monthsBetween(current, new DateTime(shownDateTime)).getMonths());
    int years = Math.abs(Years.yearsBetween(current, new DateTime(shownDateTime)).getYears());

    if (years > 0) {
      return String.format(context.getString(R.string.years), years);
    }
    if (months > 0) {
      return String.format(context.getString(R.string.months), months);
    }
    if (weeks > 0) {
      return String.format(context.getString(R.string.weeks), weeks);
    }
    if (days > 0) {
      return String.format(context.getString(R.string.days), days);
    }
    if (hours > 0) {
      return String.format(context.getString(R.string.hours), hours);
    }
    if (minutes > 0) {
      return String.format(context.getString(R.string.minutes), minutes);
    }
    if (seconds > 3) {
      return String.format(context.getString(R.string.seconds), seconds);
    }
    return context.getString(R.string.now);
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
}
