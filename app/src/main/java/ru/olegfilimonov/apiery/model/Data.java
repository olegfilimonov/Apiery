/*
 * Copyright (c) 2017 Oleg Filimonov
 */

package ru.olegfilimonov.apiery.model;

import java.util.List;

/**
 * @author Oleg Filimonov
 */

public class Data {
  private List<Event> events;
  private List<Apiery> apieries;
  private List<Tag> statuses;

  public Data() {
  }

  public Data(List<Event> events, List<Apiery> apieries, List<Tag> statuses) {
    this.events = events;
    this.apieries = apieries;
    this.statuses = statuses;
  }

  public List<Event> getEvents() {
    return events;
  }

  public void setEvents(List<Event> events) {
    this.events = events;
  }

  public List<Apiery> getApieries() {
    return apieries;
  }

  public void setApieries(List<Apiery> apieries) {
    this.apieries = apieries;
  }

  public List<Tag> getStatuses() {
    return statuses;
  }

  public void setStatuses(List<Tag> statuses) {
    this.statuses = statuses;
  }
}
