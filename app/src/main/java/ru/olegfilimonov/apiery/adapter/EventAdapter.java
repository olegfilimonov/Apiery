/*
 * Copyright (c) 2017 Oleg Filimonov
 */

package ru.olegfilimonov.apiery.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.SwipeableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.SwipeableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultAction;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultActionDefault;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultActionRemoveItem;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractSwipeableItemViewHolder;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import ru.olegfilimonov.apiery.ApieryApplication;
import ru.olegfilimonov.apiery.R;
import ru.olegfilimonov.apiery.activity.HiveActivity;
import ru.olegfilimonov.apiery.model.Event;

/**
 * @author Oleg Filimonov
 */

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> implements SwipeableItemAdapter<EventAdapter.ViewHolder> {
  private Context context;
  private List<Event> items;
  private Handler handler;

  public EventAdapter(Context context, final List<Event> items) {
    setHasStableIds(true);
    this.context = context;

    Collections.sort(items, new Comparator<Event>() {
      @Override public int compare(Event o1, Event o2) {
        return (int) (o2.getShownDateTime() - o1.getShownDateTime());
      }
    });

    handler = new Handler();
    Runnable updateRunnable = new Runnable() {
      @Override public void run() {
        try {

          Collections.sort(items, new Comparator<Event>() {
            @Override
            public int compare(Event o1, Event o2) {
              return (int) (o2.getShownDateTime() - o1.getShownDateTime());
            }
          });

          notifyDataSetChanged();
          handler.postDelayed(this, 1000);
        } catch (Throwable t) {

        }
      }
    };
    handler.postDelayed(updateRunnable, 1000);

    this.items = items;
  }

  @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_event, parent, false);
    return new ViewHolder(view);
  }

  @Override public long getItemId(int position) {
    return items.get(position).getId();
  }

  @Override public void onBindViewHolder(ViewHolder holder, int position) {
    final Event event = items.get(position);
    holder.name.setText(event.getText());
    holder.icon.setColorFilter(context.getResources().getColor(R.color.colorPrimary));
    holder.from.setText(event.getTitle());

    String format = event.getHiveId() == null ? "%s" : context.getString(R.string.event_time_format);

    boolean past = event.getShownDateTime() < System.currentTimeMillis();

    String elapsedTime = event.getElapsedTime(context);
    boolean now = elapsedTime.equals("сейчас");
    holder.time.setText(String.format(format, now ? elapsedTime : (past ? elapsedTime + " назад" : "через " + elapsedTime)));

    holder.root.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        Intent intent = new Intent(context, HiveActivity.class);
        intent.putExtra("hive_id", event.getHiveId());
        context.startActivity(intent);
      }
    });
  }

  @Override public int getItemCount() {
    return items.size();
  }

  @Override public int onGetSwipeReactionType(ViewHolder holder, int position, int x, int y) {
    return Swipeable.REACTION_CAN_SWIPE_BOTH_H;
  }

  @Override public void onSetSwipeBackground(ViewHolder holder, int position, int type) {

  }

  @Override public SwipeResultAction onSwipeItem(ViewHolder holder, int position, int result) {
    if (result == Swipeable.RESULT_CANCELED) {
      return new SwipeResultActionDefault();
    } else {
      return new SwipeRemoveItem(this, position);
    }
  }

  interface Swipeable extends SwipeableItemConstants {
  }

  private static class SwipeRemoveItem extends SwipeResultActionRemoveItem {
    private EventAdapter adapter;
    private int position;

    public SwipeRemoveItem(EventAdapter adapter, int position) {
      this.adapter = adapter;
      this.position = position;
    }

    @Override protected void onPerformAction() {
      ApieryApplication.getInstance().removeEvent(adapter.items.get(position));
      //adapter.items.remove(position);
      //adapter.notifyItemRemoved(position);
    }
  }

  class ViewHolder extends AbstractSwipeableItemViewHolder {
    @BindView(R.id.event_root) View root;
    @BindView(R.id.event_name) TextView name;
    @BindView(R.id.event_icon) ImageView icon;
    @BindView(R.id.event_from) TextView from;
    @BindView(R.id.event_time) TextView time;

    ViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }

    @Override public View getSwipeableContainerView() {
      return root;
    }
  }
}
