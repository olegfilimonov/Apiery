/*
 * Copyright (c) 2017 Oleg Filimonov
 */

package ru.olegfilimonov.apiery.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.olegfilimonov.apiery.R;
import ru.olegfilimonov.apiery.fragment.ApieryChooserFragment;
import ru.olegfilimonov.apiery.model.Apiery;

public class ApieryChooserAdapter extends RecyclerView.Adapter<ApieryChooserAdapter.ViewHolder> {

  private final List<Apiery> items;
  private final ApieryChooserFragment.ApieryChooserListener listener;
  private int selectedItem = 0;
  private Context context;

  public ApieryChooserAdapter(List<Apiery> items, ApieryChooserFragment.ApieryChooserListener listener, Context context) {
    this.items = items;
    this.listener = listener;
    this.context = context;
  }

  @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_apiery_chooser, parent, false);
    return new ViewHolder(view);
  }

  @Override public void onBindViewHolder(final ViewHolder holder, int position) {

    if (selectedItem == position) {
      holder.container.setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryLight));
    } else {
      holder.container.setBackgroundColor(context.getResources().getColor(R.color.white));
    }

    if (position == 0) {
      holder.name.setText("Добавить новый точок");
      holder.icon.setImageResource(R.drawable.ic_add_white_24dp);
    } else {
      Apiery apiery = items.get(position - 1);
      holder.name.setText(apiery.getTitle());
      holder.icon.setImageResource(R.drawable.ic_apiery);
    }
  }

  @Override public int getItemCount() {
    return items.size() + 1;
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.apiery_chooser_root) FrameLayout container;
    @BindView(R.id.apiery_chooser_icon) View dragHandle;
    @BindView(R.id.apiery_chooser_name) TextView name;
    ImageView icon;

    public ViewHolder(View v) {
      super(v);
      ButterKnife.bind(this, v);
      icon = (ImageView) dragHandle;
      container.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {

          int previous = ApieryChooserAdapter.this.selectedItem;
          selectedItem = ViewHolder.this.getAdapterPosition();

          if (listener != null) {
            listener.onItemSelected(ViewHolder.this.getAdapterPosition() == 0 ? null : items.get(ViewHolder.this.getAdapterPosition() - 1));
          }
          notifyItemChanged(previous);
          notifyItemChanged(selectedItem);
        }
      });
    }
  }
}
