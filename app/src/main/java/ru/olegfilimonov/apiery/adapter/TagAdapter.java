/*
 * Copyright (c) 2017 Oleg Filimonov
 */

package ru.olegfilimonov.apiery.adapter;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.draggable.ItemDraggableRange;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.SwipeableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.SwipeableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultAction;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultActionDefault;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultActionRemoveItem;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractDraggableSwipeableItemViewHolder;
import java.util.List;
import ru.olegfilimonov.apiery.R;
import ru.olegfilimonov.apiery.model.Tag;

public class TagAdapter extends RecyclerView.Adapter<TagAdapter.ViewHolder> implements DraggableItemAdapter<TagAdapter.ViewHolder>, SwipeableItemAdapter<TagAdapter.ViewHolder> {
  private static final String TAG = "MyDraggableItemAdapter";
  public List<Tag> tags;
  private Context context;

  public TagAdapter(Context context, List<Tag> tags) {
    this.context = context;
    this.tags = tags;

    // DraggableItemAdapter requires stable ID, and also
    // have to implement the getItemId() method appropriately.
    setHasStableIds(true);
  }

  public static boolean hitTest(View v, int x, int y) {
    final int tx = (int) (ViewCompat.getTranslationX(v) + 0.5f);
    final int ty = (int) (ViewCompat.getTranslationY(v) + 0.5f);
    final int left = v.getLeft() + tx;
    final int right = v.getRight() + tx;
    final int top = v.getTop() + ty;
    final int bottom = v.getBottom() + ty;

    return (x >= left) && (x <= right) && (y >= top) && (y <= bottom);
  }

  @Override public long getItemId(int position) {
    return tags.get(position).getId();
  }

  @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    final View v = inflater.inflate(R.layout.item_tag, parent, false);
    return new ViewHolder(v);
  }

  @Override public void onBindViewHolder(ViewHolder holder, int position) {

    final Tag tag = tags.get(position);

    // set text
    holder.name.setText(tag.getText());

    // set background resource (target view ID: container)
    final int dragState = holder.getDragStateFlags();

    if (((dragState & Draggable.STATE_FLAG_IS_UPDATED) != 0)) {
      int bgResId;

      if ((dragState & Draggable.STATE_FLAG_IS_ACTIVE) != 0) {
        bgResId = R.drawable.bg_item_dragging_active_state;

        // need to clear drawable state here to get correct appearance of the dragging tag.
        if (holder.container.getForeground() != null) {
          holder.container.getForeground().setState(new int[] {});
        }
      } else if ((dragState & Draggable.STATE_FLAG_DRAGGING) != 0) {
        bgResId = R.drawable.bg_item_dragging_state;
      } else {
        bgResId = R.drawable.bg_item_normal_state;
      }

      holder.container.setBackgroundResource(bgResId);
    }
  }

  @Override public int getItemCount() {
    return tags.size();
  }

  @Override public void onMoveItem(int fromPosition, int toPosition) {
    Log.d(TAG, "onMoveItem(fromPosition = " + fromPosition + ", toPosition = " + toPosition + ")");

    if (fromPosition == toPosition) {
      return;
    }

    Tag tag = tags.remove(fromPosition);
    tags.add(toPosition, tag);
    notifyItemMoved(fromPosition, toPosition);
  }

  @Override public boolean onCheckCanStartDrag(ViewHolder holder, int position, int x, int y) {
    // x, y --- relative from the itemView's top-left
    final View containerView = holder.container;
    final View dragHandleView = holder.dragHandle;

    final int offsetX = containerView.getLeft() + (int) (ViewCompat.getTranslationX(containerView) + 0.5f);
    final int offsetY = containerView.getTop() + (int) (ViewCompat.getTranslationY(containerView) + 0.5f);

    return hitTest(dragHandleView, x - offsetX, y - offsetY);
  }

  @Override public ItemDraggableRange onGetItemDraggableRange(ViewHolder holder, int position) {
    // no drag-sortable range specified
    return null;
  }

  @Override public boolean onCheckCanDrop(int draggingPosition, int dropPosition) {
    return true;
  }

  @Override public int onGetSwipeReactionType(ViewHolder holder, int position, int x, int y) {
    return SwipeableItemConstants.REACTION_CAN_SWIPE_LEFT;
  }

  @Override public void onSetSwipeBackground(ViewHolder holder, int position, int type) {

  }

  @Override public SwipeResultAction onSwipeItem(ViewHolder holder, int position, int result) {
    if (result == EventAdapter.Swipeable.RESULT_CANCELED) {
      return new SwipeResultActionDefault();
    } else {
      return new TagAdapter.SwipeRemoveItem(this, position);
    }
  }

  // NOTE: Make accessible with short name
  private interface Draggable extends DraggableItemConstants {
  }

  static class SwipeRemoveItem extends SwipeResultActionRemoveItem {
    private TagAdapter adapter;
    private int position;

    public SwipeRemoveItem(TagAdapter adapter, int position) {
      this.adapter = adapter;
      this.position = position;
    }

    @Override protected void onPerformAction() {
      adapter.tags.remove(position);
      adapter.notifyItemRemoved(position);
    }
  }

  public static class ViewHolder extends AbstractDraggableSwipeableItemViewHolder {
    @BindView(R.id.tag_root) FrameLayout container;
    @BindView(R.id.tag_icon) View dragHandle;
    @BindView(R.id.tag_name) TextView name;
    ImageView icon;

    public ViewHolder(View v) {
      super(v);
      ButterKnife.bind(this, v);
      icon = (ImageView) dragHandle;
    }

    @Override public View getSwipeableContainerView() {
      return container;
    }
  }
}