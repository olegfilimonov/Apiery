/*
 * Copyright (c) 2017 Oleg Filimonov
 */

package ru.olegfilimonov.apiery.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import java.util.ArrayList;
import java.util.List;
import ru.olegfilimonov.apiery.ApieryApplication;
import ru.olegfilimonov.apiery.R;
import ru.olegfilimonov.apiery.model.Apiery;
import ru.olegfilimonov.apiery.model.Hive;
import ru.olegfilimonov.apiery.model.Tag;

/**
 * @author Oleg Filimonov
 */
public class HiveAdapter extends RecyclerView.Adapter<HiveAdapter.ViewHolder> {
  @Nullable private ApieryListAdapter.Listener listener;
  private Context context;
  private Apiery apiery;
  private List<Hive> hives;
  private boolean showAll;

  public HiveAdapter(@Nullable ApieryListAdapter.Listener listener, Context context, List<Hive> hives, boolean showAll) {
    this.listener = listener;
    this.context = context;
    this.hives = hives;
    this.showAll = showAll;
  }

  public HiveAdapter(ApieryListAdapter.Listener listener, Context context, boolean b) {
    this(listener, context, new ArrayList<Hive>(), b);
  }

  void setHives(Apiery apiery, List<Hive> hives) {
    this.apiery = apiery;
    this.hives = hives;
    notifyDataSetChanged();
  }

  @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hive, parent, false);
    return new ViewHolder(view);
  }

  @Override public void onBindViewHolder(ViewHolder holder, int position) {
    Hive hive = hives.get(position);

    holder.name.setText(hive.getName());
    holder.note.setVisibility(hive.getNote() == null ? View.GONE : View.VISIBLE);
    holder.note.setText(hive.getNote());
    Tag tag = ApieryApplication.getInstance().getStatus(hive.getTagsId());
    if (tag == null) {
      holder.stepIcon.setImageDrawable(null);
    } else {
      //            holder.stepIcon.setImageDrawable(TextDrawable.builder().buildRound(String.valueOf(tag.getIconId()), tag.getColor()));
    }
  }

  @Override public int getItemCount() {
    return (showAll || hives.size() <= 3) ? hives.size() : 3;
  }

  public boolean isShowAll() {
    return showAll;
  }

  void setShowAll(boolean showAll) {
    if (showAll == this.showAll || hives.size() <= 3) return;
    this.showAll = showAll;
    if (showAll) {
      notifyItemRangeInserted(3, hives.size());
    } else {
      notifyItemRangeRemoved(3, hives.size());
    }
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.hive_name) TextView name;
    @BindView(R.id.hive_note) TextView note;
    @BindView(R.id.hive_step_icon) ImageView stepIcon;

    ViewHolder(View itemView) {
      super(itemView);
      itemView.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {
          if (listener != null) {
            listener.onClick(apiery, hives.get(ViewHolder.this.getAdapterPosition()));
          }
        }
      });
      ButterKnife.bind(this, itemView);
    }
  }
}
