/*
 * Copyright (c) 2017 Oleg Filimonov
 */

package ru.olegfilimonov.apiery.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import java.util.List;
import ru.olegfilimonov.apiery.R;
import ru.olegfilimonov.apiery.model.Apiery;
import ru.olegfilimonov.apiery.model.Hive;

/**
 */
public class ApieryListAdapter extends RecyclerView.Adapter<ApieryListAdapter.ViewHolder> {
  private final List<Apiery> apieries;
  @Nullable private Listener listener;
  private android.content.Context context;

  public ApieryListAdapter(@Nullable Listener listener, List<Apiery> items, Context context) {
    this.listener = listener;
    apieries = items;
    this.context = context;
  }

  public void updateHive(Hive newHive) {
    for (Apiery apiery : apieries) {
      for (Hive hive : apiery.getHives()) {
        if (hive.getId() == newHive.getId()) {
          hive.update(newHive);
          notifyDataSetChanged();
          return;
        }
      }
    }
  }

  @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_apiery, parent, false);
    ViewHolder holder = new ViewHolder(view);
    holder.hives.setLayoutManager(new LinearLayoutManager(context));
    HiveAdapter adapter = new HiveAdapter(listener, context, false);
    holder.hives.setAdapter(adapter);
    return holder;
  }

  @Override public void onBindViewHolder(final ViewHolder holder, final int position) {
    final Apiery apiery = apieries.get(position);

    holder.title.setText(apiery.getTitle());

    HiveAdapter adapter = (HiveAdapter) holder.hives.getAdapter();
    adapter.setHives(apiery, apiery.getHives());
    adapter.setShowAll(apiery.isExpanded());

    holder.showMoreLayout.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View c) {
        boolean newState = !apiery.isExpanded();
        apiery.setExpanded(newState);
        ApieryListAdapter.this.notifyItemChanged(position);
      }
    });

    boolean showAll = apiery.isExpanded();

    holder.moreText.setText(showAll ? "Свернуть полный список" : "Открыть полный список (" + apiery.getHives().size() + ")");
    holder.moreIcon.setRotation(showAll ? 180f : 0f);

    holder.showMoreLayout.setVisibility(apiery.getHives().size() <= 3 ? View.GONE : View.VISIBLE);
  }

  @Override public int getItemCount() {
    return apieries.size();
  }

  public void setListener(@Nullable Listener listener) {
    this.listener = listener;
  }

  public interface Listener {

    void onClick(Apiery apiery, Hive hive);
  }

  class ViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.apiery_title) TextView title;
    @BindView(R.id.hives_recyclerview) RecyclerView hives;
    @BindView(R.id.show_more_layout) ConstraintLayout showMoreLayout;
    @BindView(R.id.apiery_hives_more) TextView moreText;
    @BindView(R.id.apiery_hives_more_icon) ImageView moreIcon;

    ViewHolder(View view) {
      super(view);
      ButterKnife.bind(this, view);
    }
  }
}
