/*
 * Copyright (c) 2017 Oleg Filimonov
 */

package ru.olegfilimonov.apiery.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntro2Fragment;
import com.github.paolorotolo.appintro.AppIntroFragment;
import ru.olegfilimonov.apiery.R;

public class IntroActivity extends AppIntro {
  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Note here that we DO NOT use setContentView();

    // Add your slide fragments here.
    // AppIntro will automatically generate the dots indicator and buttons.
    //        addSlide(firstFragment);
    //        addSlide(secondFragment);
    //        addSlide(thirdFragment);
    //        addSlide(fourthFragment);

    // Instead of fragments, you can also use our default slide
    // Just set a title, description, background and image. AppIntro will do the rest.
    addSlide(AppIntro2Fragment.newInstance("Привет", "Тут будет описание того, что приложение может сделать", R.drawable.example_slide_image,
        getResources().getColor(R.color.colorPrimary)));
    addSlide(AppIntro2Fragment.newInstance("Слайд 2", "Здесь можно попросить permissions", R.drawable.example_slide_image, getResources().getColor(R.color.colorPrimary)));
    addSlide(AppIntroFragment.newInstance("Финальный слайд", "Финальный слайд", R.drawable.example_slide_image, getResources().getColor(R.color.colorPrimary)));

    // OPTIONAL METHODS
    // Override bar/separator color.
    //        setBarColor(Color.parseColor("#3F51B5"));
    //        setSeparatorColor(Color.parseColor("#2196F3"));

    // Hide Skip/Done button.
    showSkipButton(true);
    setProgressButtonEnabled(true);

    // Turn vibration on and set intensity.
    // NOTE: you will probably need to ask VIBRATE permission in Manifest.
    setVibrate(true);
    setVibrateIntensity(10);
  }

  @Override public void onSkipPressed(Fragment currentFragment) {
    super.onSkipPressed(currentFragment);
    finish();
  }

  @Override public void onDonePressed(Fragment currentFragment) {
    super.onDonePressed(currentFragment);
    finish();
  }

  @Override public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
    super.onSlideChanged(oldFragment, newFragment);
    // Do something when the slide changes.
  }
}