/*
 * Copyright (c) 2017 Oleg Filimonov
 */

package ru.olegfilimonov.apiery.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.h6ah4i.android.widget.advrecyclerview.draggable.RecyclerViewDragDropManager;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.RecyclerViewSwipeManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.WrapperAdapterUtils;
import java.util.List;
import ru.olegfilimonov.apiery.ApieryApplication;
import ru.olegfilimonov.apiery.R;
import ru.olegfilimonov.apiery.adapter.TagAdapter;
import ru.olegfilimonov.apiery.model.Tag;

public class TagsActivity extends AppCompatActivity {
  @BindView(R.id.tags_recyclerview) RecyclerView recyclerView;

  private RecyclerView.LayoutManager mLayoutManager;
  private RecyclerView.Adapter<TagAdapter.ViewHolder> mAdapter;
  private RecyclerView.Adapter mWrappedAdapter;
  private RecyclerViewDragDropManager mRecyclerViewDragDropManager;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_tags);
    ButterKnife.bind(this);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    toolbar.setTitle("Текущие ярлыки:");
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowHomeEnabled(true);

    recyclerView.setLayoutManager(new LinearLayoutManager(this));

    mRecyclerViewDragDropManager = new RecyclerViewDragDropManager();

    RecyclerViewSwipeManager swipeManager = new RecyclerViewSwipeManager();

    //adapter
    List<Tag> tags = ApieryApplication.getInstance().getTags();
    mAdapter = new TagAdapter(this, tags);

    mWrappedAdapter = swipeManager.createWrappedAdapter(mRecyclerViewDragDropManager.createWrappedAdapter(mAdapter));      // wrap for dragging

    //        final GeneralItemAnimator animator = new DraggableItemAnimator();

    recyclerView.setAdapter(mWrappedAdapter);  // requires *wrapped* adapter

    mRecyclerViewDragDropManager.attachRecyclerView(recyclerView);
    swipeManager.attachRecyclerView(recyclerView);
  }

  @Override public void onPause() {
    mRecyclerViewDragDropManager.cancelDrag();
    super.onPause();
  }

  @Override public void onDestroy() {
    if (mRecyclerViewDragDropManager != null) {
      mRecyclerViewDragDropManager.release();
      mRecyclerViewDragDropManager = null;
    }

    if (recyclerView != null) {
      recyclerView.setItemAnimator(null);
      recyclerView.setAdapter(null);
      recyclerView = null;
    }

    if (mWrappedAdapter != null) {
      WrapperAdapterUtils.releaseAll(mWrappedAdapter);
      mWrappedAdapter = null;
    }
    mAdapter = null;
    mLayoutManager = null;

    super.onDestroy();
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      onBackPressed();
      return true;
    }
    return false;
  }
}
