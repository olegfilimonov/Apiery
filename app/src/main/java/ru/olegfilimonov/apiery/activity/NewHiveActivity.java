/*
 * Copyright (c) 2017 Oleg Filimonov
 */

package ru.olegfilimonov.apiery.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import com.google.android.gms.maps.model.LatLng;
import ru.olegfilimonov.apiery.ApieryApplication;
import ru.olegfilimonov.apiery.R;
import ru.olegfilimonov.apiery.fragment.ApieryChooserFragment;
import ru.olegfilimonov.apiery.fragment.MapFragment;
import ru.olegfilimonov.apiery.fragment.NewHiveFragment;
import ru.olegfilimonov.apiery.model.Hive;

public class NewHiveActivity extends AppCompatActivity implements NewHiveFragment.HiveFragmentListener, MapFragment.MapFragmentListener {

  /**
   * The {@link android.support.v4.view.PagerAdapter} that will provide
   * fragments for each of the sections. We use a
   * {@link FragmentPagerAdapter} derivative, which will keep every
   * loaded fragment in memory. If this becomes too memory intensive, it
   * may be best to switch to a
   * {@link android.support.v4.app.FragmentStatePagerAdapter}.
   */
  private SectionsPagerAdapter mSectionsPagerAdapter;

  /**
   * The {@link ViewPager} that will host the section contents.
   */
  private ViewPager mViewPager;
  private NewHiveFragment newHiveFragment;
  private MapFragment mapFragment;
  private Toolbar toolbar;
  private Menu menu;
  private ApieryChooserFragment apieryChooseFragment;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_new_hive);

    toolbar = (Toolbar) findViewById(R.id.toolbar);
    toolbar.setTitle("Выбери место улья");
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowHomeEnabled(true);
    // Create the adapter that will return a fragment for each of the three
    // primary sections of the activity.
    mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

    // Set up the ViewPager with the sections adapter.
    mViewPager = (ViewPager) findViewById(R.id.container);
    mViewPager.setAdapter(mSectionsPagerAdapter);
    newHiveFragment = NewHiveFragment.newInstance();
    mapFragment = MapFragment.newInstance(true);
    apieryChooseFragment = ApieryChooserFragment.newInstance();
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    this.menu = menu;
    getMenuInflater().inflate(R.menu.menu_new_hive, this.menu);
    return true;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    if (id == R.id.action_ok) {

      if (mViewPager.getCurrentItem() == 0) {
        selectItem(1);
      } else if (mViewPager.getCurrentItem() == 1) {
        newHiveFragment.setLatLng(mapFragment.getLocation());
        long apieryId =
            apieryChooseFragment.getSelectedApiery() == null ? ApieryApplication.getInstance().getNextUnusedApieryId() : apieryChooseFragment.getSelectedApiery().getId();
        newHiveFragment.setApieryId(apieryId);

        newHiveFragment.setupView();
        selectItem(2);
      } else if (mViewPager.getCurrentItem() == 2) {

        Hive hive = newHiveFragment.getHive();
        Intent intent = new Intent();
        intent.putExtra("hive", hive);
        setResult(RESULT_OK, intent);
        finish();
      }

      return true;
    }

    if (id == android.R.id.home) {
      if (mViewPager.getCurrentItem() == 0) {
        onBackPressed();
      } else {
        selectItem(0);
      }
    }

    return super.onOptionsItemSelected(item);
  }

  @Override public void onBackPressed() {
    if (mViewPager.getCurrentItem() == 0) {
      super.onBackPressed();
    } else {
      selectItem(mViewPager.getCurrentItem() - 1);
    }
  }

  private void selectItem(int i) {
    mViewPager.setCurrentItem(i);
    switch (i) {
      case 0:
        getSupportActionBar().setTitle("Выбери место улья");
        menu.findItem(R.id.action_ok).setTitle("ДАЛЕЕ");
        break;
      case 1:
        getSupportActionBar().setTitle("Выбери точок улья");
        menu.findItem(R.id.action_ok).setTitle("ДАЛЕЕ");
        break;
      default:
        getSupportActionBar().setTitle("Персонализация");
        menu.findItem(R.id.action_ok).setTitle("ОК");
        break;
    }
  }

  @Override public void onHiveSubmit(Hive hive) {

  }

  @Override public void onCameraUpdate(LatLng latLng) {
  }

  /**
   * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
   * one of the sections/tabs/pages.
   */
  public class SectionsPagerAdapter extends FragmentPagerAdapter {

    public SectionsPagerAdapter(FragmentManager fm) {
      super(fm);
    }

    @Override public Fragment getItem(int position) {
      switch (position) {
        case 0:
          return mapFragment;
        case 1:
          return apieryChooseFragment;
        case 2:
          return newHiveFragment;
        default:
          return null;
      }
    }

    @Override public int getCount() {
      return 3;
    }
  }
}
