/*
 * Copyright (c) 2017 Oleg Filimonov
 */

package ru.olegfilimonov.apiery.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import java.util.List;
import org.joda.time.DateTime;
import ru.olegfilimonov.apiery.ApieryApplication;
import ru.olegfilimonov.apiery.R;
import ru.olegfilimonov.apiery.adapter.EventAdapter;
import ru.olegfilimonov.apiery.model.Apiery;
import ru.olegfilimonov.apiery.model.Event;
import ru.olegfilimonov.apiery.model.Hive;
import ru.olegfilimonov.apiery.model.Tag;

public class HiveActivity extends AppCompatActivity {
  @BindView(R.id.hivedetail_added) TextView added;
  @BindView(R.id.hivedetail_apiery_link) TextView apieryLink;
  @BindView(R.id.hivedetail_location) TextView location;
  @BindView(R.id.hivedetail_name) TextInputLayout name;
  @BindView(R.id.hivedetail_note) TextInputLayout note;
  @BindView(R.id.hivedetail_events_recyclerview) RecyclerView eventsRecyclerview;
  SupportMapFragment fragment;
  private Hive hive;
  private List<Tag> tags;

  public Tag getTag(long tagId) {
    for (Tag tag : tags) {
      if (tag.getId() == tagId) return tag;
    }
    return null;
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_hive);
    ButterKnife.bind(this);
    tags = ApieryApplication.getInstance().getTags();
    long hive_id = getIntent().getLongExtra("hive_id", 0);
    hive = ApieryApplication.getInstance().getHiveById(hive_id);

    //        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager());
    //        pager.setAdapter(adapter);
    //        pager.setCurrentItem((int) hive.getTagsId());
    //        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
    //            @Override
    //            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    //
    //            }
    //
    //            @Override
    //            public void onPageSelected(int position) {
    //                hive.setTagsId(position);
    //            }
    //
    //            @Override
    //            public void onPageScrollStateChanged(int state) {
    //
    //            }
    //        });

    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    toolbar.setTitle("Подробности");

    DateTime dateTime = new DateTime(hive.getCreatedDateTime());

    added.setText(String.format("Добавлен %s", dateTime.toString("dd.MM.yyyy hh:mm")));

    Apiery linkedApiery = ApieryApplication.getInstance().getApieryById(hive.getApieryId());
    apieryLink.setText(linkedApiery.getTitle());

    location.setText(String.valueOf(hive.getLocation().latitude) + "; " + String.valueOf(hive.getLocation().longitude));
    name.getEditText().setText(hive.getName());
    note.getEditText().setText(hive.getNote());

    List<Event> events = ApieryApplication.getInstance().getEventsByHiveId(hive.getId());

    eventsRecyclerview.setLayoutManager(new LinearLayoutManager(this));
    eventsRecyclerview.setAdapter(new EventAdapter(this, events));
    eventsRecyclerview.setNestedScrollingEnabled(false);

    note.getEditText().addTextChangedListener(new TextWatcher() {
      @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
        hive.setNote(s.toString());
      }

      @Override public void afterTextChanged(Editable s) {

      }
    });
    name.getEditText().addTextChangedListener(new TextWatcher() {
      @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
        hive.setName(s.toString());
      }

      @Override public void afterTextChanged(Editable s) {

      }
    });
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowHomeEnabled(true);

    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

    fragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
    fragment.getMapAsync(new OnMapReadyCallback() {
      @Override public void onMapReady(GoogleMap googleMap) {
        LatLng latLng = new LatLng(hive.getLocation().getLatitude(), hive.getLocation().getLongitude());
        googleMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.hive)));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
      }
    });
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.save, menu);
    return true;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      onBackPressed();
      return true;
    } else if (item.getItemId() == R.id.action_save) {
      Intent intent = new Intent();
      intent.putExtra("hive", hive);
      setResult(RESULT_OK, intent);
      finish();
      return true;
    }
    return false;
  }

  public static class TagFragment extends Fragment {
    @BindView(R.id.tag_icon) ImageView icon;
    @BindView(R.id.tag_name) TextView name;
    private Tag tag;

    public static TagFragment newInstance(Tag tag) {
      TagFragment fragment = new TagFragment();
      fragment.setTag(tag);
      return fragment;
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.item_tag, container, false);
      ButterKnife.bind(this, rootView);

      // set text
      name.setText(tag.getText());

      return rootView;
    }

    public void setTag(Tag tag) {
      this.tag = tag;
    }
  }

  public class PagerAdapter extends FragmentStatePagerAdapter {
    public PagerAdapter(FragmentManager fm) {
      super(fm);
    }

    @Override public Fragment getItem(int position) {
      Tag tag = getTag(position);

      return TagFragment.newInstance(tag);
    }

    @Override public int getCount() {
      return tags.size();
    }
  }
}
