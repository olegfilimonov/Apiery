/*
 * Copyright (c) 2017 Oleg Filimonov
 */

package ru.olegfilimonov.apiery.activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.amulyakhare.textdrawable.TextDrawable;
import com.bumptech.glide.Glide;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.github.mikephil.charting.utils.Utils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import java.util.Calendar;
import org.joda.time.DateTime;
import ru.olegfilimonov.apiery.AlarmReceiver;
import ru.olegfilimonov.apiery.ApieryApplication;
import ru.olegfilimonov.apiery.R;
import ru.olegfilimonov.apiery.adapter.ApieryListAdapter;
import ru.olegfilimonov.apiery.fragment.ApieryFragment;
import ru.olegfilimonov.apiery.fragment.EventsFragment;
import ru.olegfilimonov.apiery.fragment.MapFragment;
import ru.olegfilimonov.apiery.model.Apiery;
import ru.olegfilimonov.apiery.model.Event;
import ru.olegfilimonov.apiery.model.Hive;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
  public static final int RC_NEW_HIVE = 521;
  @BindView(R.id.menu_item_1) FloatingActionButton fab1;
  @BindView(R.id.menu_item_2) FloatingActionButton fab2;
  @BindView(R.id.fab_menu) FloatingActionMenu fab;
  @BindView(R.id.drawer_layout) DrawerLayout drawer;
  @BindView(R.id.toolbar) Toolbar toolbar;
  @BindView(R.id.coordinator_layout) CoordinatorLayout coordinatorLayout;
  private AppCompatButton headerLoginButton;
  private RelativeLayout contentSignedIn;
  private TextView nameView;
  private TextView emailView;
  private CircleImageView avatarView;
  private ViewPager pager;
  private LinearLayout contentSignedOut;
  private boolean loggedIn = false;
  private ProgressDialog progressDialog;
  private PagerAdapter adapter;
  private DrawerLayout drawerLayout;
  private NavigationView navigationView;
  private String TAG = "MAIN";
  // Date to be used in creation of event in dialog
  private DateTime dialogDateTime;
  private Hive dialogHive;
  private AlertDialog hiveChooserDialog;

  private static void resizeDialogToMatchParent(AlertDialog dialog) {
    //Grab the window of the dialog, and change the width
    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
    Window window = dialog.getWindow();
    lp.copyFrom(window.getAttributes());
    //This makes the dialog take up the full width
    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
    window.setAttributes(lp);
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);

    setupToolbar();
    setupFab();
    setupPagerLayout();
    setupDrawerLayout();
  }

  private void setupToolbar() {
    setSupportActionBar(toolbar);
  }

  private void setupDrawerLayout() {
    ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
    drawer.addDrawerListener(toggle);
    toggle.syncState();

    navigationView = (NavigationView) findViewById(R.id.nav_view);
    navigationView.setNavigationItemSelectedListener(this);

    View view = navigationView.getHeaderView(0);
    headerLoginButton = (AppCompatButton) view.findViewById(R.id.header_login);
    contentSignedIn = (RelativeLayout) view.findViewById(R.id.content_singed_in);
    contentSignedOut = (LinearLayout) view.findViewById(R.id.content_signed_out);
    nameView = (TextView) view.findViewById(R.id.profile_name);
    emailView = (TextView) view.findViewById(R.id.profile_email);
    avatarView = (CircleImageView) view.findViewById(R.id.profile_image);
    ImageView exitView = (ImageView) view.findViewById(R.id.profile_exit);

    exitView.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view12) {
        FirebaseAuth.getInstance().signOut();
        refreshPages();
        onResume();
      }
    });
    headerLoginButton.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view1) {
        startActivity(new Intent(MainActivity.this, LoginActivity.class));
      }
    });
  }

  private void setupPagerLayout() {
    adapter = new PagerAdapter(getSupportFragmentManager());
    pager = (ViewPager) findViewById(R.id.content_main);
    pager.setAdapter(adapter);
    pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
      @Override public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

      }

      @Override public void onPageSelected(int position) {
        if (position == 1) {
          fab.hideMenuButton(true);
        } else {
          fab.showMenuButton(true);
        }

        switch (position) {
          case 0:
            navigationView.getMenu().findItem(R.id.nav_events).setChecked(true);
            break;

          case 1:
            navigationView.getMenu().findItem(R.id.nav_map).setChecked(true);
            break;

          case 2:
            navigationView.getMenu().findItem(R.id.nav_list).setChecked(true);
            break;
        }
      }

      @Override public void onPageScrollStateChanged(int state) {

      }
    });

    TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
    tabLayout.setupWithViewPager(pager);
  }

  private void setupFab() {
    fab1.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {

        //fab.hideMenu(true);

        // MAIN DIALOG VIEW
        final LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
        View root = inflater.inflate(R.layout.dialog_new_event, null);

        final TextInputLayout name = (TextInputLayout) root.findViewById(R.id.new_event_name);
        final TextInputLayout date = (TextInputLayout) root.findViewById(R.id.new_event_date);
        final TextInputLayout hive = (TextInputLayout) root.findViewById(R.id.new_event_hive);

        ImageView dateIcon = (ImageView) root.findViewById(R.id.new_event_date_icon);
        ImageView hiveIcon = (ImageView) root.findViewById(R.id.new_event_hive_icon);

        // Date chooser
        View.OnClickListener dateChooserListener = new View.OnClickListener() {
          @Override public void onClick(View view1) {
            final Calendar now = Calendar.getInstance();
            DatePickerDialog dpd = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
              @Override public void onDateSet(DatePickerDialog view2, final int year, final int monthOfYear, final int dayOfMonth) {

                // Date picker success
                TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
                  @Override public void onTimeSet(TimePickerDialog view3, int hourOfDay, int minute, int second) {

                    // Success
                    date.getEditText().setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year + " " + hourOfDay + ":" + minute);

                    dialogDateTime = new DateTime(year, monthOfYear + 1, dayOfMonth, hourOfDay, minute, second);
                  }
                }, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), true).show(MainActivity.this.getFragmentManager(), "Выберите время");

                dialogDateTime = new DateTime(year, monthOfYear + 1, dayOfMonth, 0, 0);
                date.getEditText().setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
              }
            }, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
            dpd.show(MainActivity.this.getFragmentManager(), "Выберите дату");
          }
        };

        dateIcon.setOnClickListener(dateChooserListener);
        date.setOnClickListener(dateChooserListener);
        date.getEditText().setOnClickListener(dateChooserListener);

        // Hive chooser
        View.OnClickListener hiveChooserListener = new View.OnClickListener() {
          @Override public void onClick(View view1) {

            if (hiveChooserDialog != null) {
              hiveChooserDialog.dismiss();
              hiveChooserDialog = null;
            }

            View hiveListRoot = inflater.inflate(R.layout.dialog_hive_select, null);
            final ApieryFragment fragment = (ApieryFragment) MainActivity.this.getSupportFragmentManager().findFragmentById(R.id.hive_select_container);

            // HIVE CHOOSER DIALOG
            ApieryListAdapter.Listener listener = new ApieryListAdapter.Listener() {
              @Override public void onClick(Apiery apiery1, Hive hive1) {
                MainActivity.this.dialogHive = hive1;
                hive.getEditText().setText(hive1.getName());
                MainActivity.this.getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                hiveChooserDialog.dismiss();
              }
            };

            fragment.setListener(listener);

            hiveChooserDialog = new AlertDialog.Builder(MainActivity.this, R.style.AppTheme_Dialog).setView(hiveListRoot).setTitle("Выберете улей для события").create();

            hiveChooserDialog.show();

            resizeDialogToMatchParent(hiveChooserDialog);
          }
        };

        hiveIcon.setOnClickListener(hiveChooserListener);
        hive.setOnClickListener(hiveChooserListener);
        hive.getEditText().setOnClickListener(hiveChooserListener);

        final AlertDialog mainDialog;

        mainDialog = new AlertDialog.Builder(MainActivity.this, R.style.AppTheme_Dialog).setTitle("Новое событие")
            .setView(root)
            .setPositiveButton("ОК", null)
            .setNegativeButton("ОТМЕНА", new DialogInterface.OnClickListener() {
              @Override public void onClick(DialogInterface dialogInterface, int i) {
              }
            })
            .create();

        mainDialog.setOnShowListener(new DialogInterface.OnShowListener() {
          @Override public void onShow(DialogInterface dialogInterface) {
            Button b = mainDialog.getButton(AlertDialog.BUTTON_POSITIVE);
            b.setOnClickListener(new View.OnClickListener() {
              @Override public void onClick(View view1) {

                Event event = new Event();
                event.setText(name.getEditText().getText().toString());
                if (dialogDateTime != null) {
                  event.setShownDateTime(dialogDateTime.getMillis());
                }
                if (dialogHive != null) {
                  event.setHiveId(dialogHive.getId());
                  event.setTitle(dialogHive.getName());
                  dialogHive = null;
                }

                event.setId(ApieryApplication.getInstance().getNextUnusedEventId());

                ApieryApplication.getInstance().addEvent(event);

                scheduleAlarm(event.getShownDateTime(), event.getTitle(), event.getText(), event.getHiveId() == null ? null : String.valueOf(event.getHiveId()));

                MainActivity.this.refreshPages();

                mainDialog.dismiss();

                return;
              }
            });
          }
        });

        mainDialog.show();
        resizeDialogToMatchParent(mainDialog);
      }
    });

    fab2.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        //fab.hideMenu(true);

        Intent intent = new Intent(MainActivity.this, NewHiveActivity.class);
        MainActivity.this.startActivityForResult(intent, RC_NEW_HIVE);
      }
    });
  }

  public void scheduleAlarm(long time, String title, String message, @Nullable String hiveId) {
    // Create an Intent and set the class that will execute when the Alarm triggers. Here we have
    // specified AlarmReceiver in the Intent. The onReceive() method of this class will execute when the broadcast from your alarm is received.
    Intent intentAlarm = new Intent(this, AlarmReceiver.class);
    intentAlarm.putExtra("title", title);
    intentAlarm.putExtra("message", message);
    if (hiveId != null) intentAlarm.putExtra("hiveId", hiveId);

    // Get the Alarm Service.
    AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

    // Set the alarm for a particular time.
    alarmManager.set(AlarmManager.RTC_WAKEUP, time, PendingIntent.getBroadcast(this, 1, intentAlarm, PendingIntent.FLAG_UPDATE_CURRENT));
  }

  private void showProgress(final boolean show) {
    if (show) {
      if (progressDialog != null) return;
      progressDialog = new ProgressDialog(MainActivity.this, R.style.AppTheme_Dialog);
      progressDialog.setIndeterminate(true);
      progressDialog.setCancelable(false);
      progressDialog.setMessage(getString(R.string.data_progress_message));
      progressDialog.show();
    } else {
      progressDialog.dismiss();
      progressDialog = null;
    }
  }

  @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (requestCode == RC_NEW_HIVE) {
      if (resultCode == RESULT_OK) {

        ApieryApplication.getInstance().addHive((Hive) data.getParcelableExtra("hive"));

        refreshPages();
      }
    }
  }

  @Override protected void onResume() {
    super.onResume();

    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

    if (user != null) {

      contentSignedIn.setVisibility(View.VISIBLE);
      headerLoginButton.setVisibility(View.GONE);
      contentSignedOut.setVisibility(View.GONE);

      // Name, email address, and profile photo Url
      String name = user.getDisplayName();
      String email = user.getEmail();
      Uri photoUrl = user.getPhotoUrl();

      if (user.getDisplayName() == null) {
        nameView.setVisibility(View.GONE);
      } else {
        nameView.setText(name);
        nameView.setVisibility(View.VISIBLE);
      }

      if (photoUrl == null) {
        String substring = email.substring(0, 2);
        TextDrawable textDrawable = TextDrawable.builder()
            .beginConfig()
            .width((int) Utils.convertDpToPixel(56))
            .height((int) Utils.convertDpToPixel(56))
            .endConfig()
            .buildRect(substring, getResources().getColor(R.color.colorAccent));
        avatarView.setImageDrawable(textDrawable);
      } else {
        Glide.with(this).load(photoUrl).into(avatarView);
      }

      emailView.setText(email);

      if (!loggedIn) {
        showProgress(true);

        ApieryApplication.getInstance().loadDataFromServer(new ValueEventListener() {
          @Override public void onDataChange(DataSnapshot dataSnapshot) {
            showProgress(false);
            refreshPages();
          }

          @Override public void onCancelled(DatabaseError databaseError) {
            showProgress(false);
            Toast.makeText(MainActivity.this, "Cannot load your data", Toast.LENGTH_SHORT).show();
            FirebaseAuth.getInstance().signOut();
            onResume();
          }
        });
      }

      loggedIn = true;
    } else {
      contentSignedOut.setVisibility(View.VISIBLE);
      headerLoginButton.setVisibility(View.VISIBLE);
      contentSignedIn.setVisibility(View.GONE);

      loggedIn = false;
    }
  }

  @Override public void onBackPressed() {
    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    if (drawer.isDrawerOpen(GravityCompat.START)) {
      drawer.closeDrawer(GravityCompat.START);
    } else {
      super.onBackPressed();
    }
  }

  public void refreshPages() {
    //        adapter.notifyDataSetChanged();
    pager.setAdapter(adapter);
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    //        if (id == R.id.action_settings) {
    //            return true;
    //        }

    return super.onOptionsItemSelected(item);
  }

  boolean onLongClick() {

    //  Initialize SharedPreferences
    SharedPreferences getPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

    //  Create a new boolean and preference and set it to true
    boolean isFirstStart = getPrefs.getBoolean("firstStart", true);

    //  Make a new preferences editor
    SharedPreferences.Editor e = getPrefs.edit();

    //  If the activity has never started before...
    // TODO: 21/12/2016 Remove on release
    if (isFirstStart || true) {

      //  Launch app intro
      Intent i = new Intent(MainActivity.this, IntroActivity.class);
      startActivity(i);

      //  Edit preference to make it false because we don't want this to run again
      e.putBoolean("firstStart", false);

      //  Apply changes
      e.apply();
    }

    int[] out = new int[2];
    fab.getLocationOnScreen(out);
    // This tapTargetSequence will tell us when interesting(tm) events happen in regards
    TapTargetSequence tapTargetSequence = new TapTargetSequence(this).targets(
        TapTarget.forView(findViewById(R.id.fab_menu_dummy), "Чтобы добавить твой первый улей, нажми сюда", "Также с помощью этой кнопки можно добавить заметки и события")
            .transparentTarget(true)
            .targetRadius(60), TapTarget.forView(fab2, "Теперь выбери добавить улей", "Возможность распределить улей по точкам будет далее").tintTarget(false))
        .listener(new TapTargetSequence.Listener() {
          // This tapTargetSequence will tell us when interesting(tm) events happen in regards
          // to the sequence
          @Override public void onSequenceFinish() {
            fab2.callOnClick();
          }

          @Override public void onSequenceCanceled(TapTarget lastTarget) {
            // Boo
          }
        });

    tapTargetSequence.start();
    fab.open(true);
    return true;
  }

  @SuppressWarnings("StatementWithEmptyBody") @Override public boolean onNavigationItemSelected(MenuItem item) {
    // Handle navigation view item clicks here.
    int id = item.getItemId();

    switch (id) {
      case R.id.nav_cheatsheet:
        Intent intent = new Intent(this, TagsActivity.class);
        startActivity(intent);
        break;
      case R.id.nav_tutorial:
        onLongClick();
        break;
      case R.id.nav_events:
        pager.setCurrentItem(0, true);
        break;
      case R.id.nav_list:
        pager.setCurrentItem(2, true);
        break;
      case R.id.nav_map:
        pager.setCurrentItem(1, true);
        break;
      case R.id.nav_rate:

        break;
    }

    drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
    drawerLayout.closeDrawer(GravityCompat.START);
    return true;
  }

  private class PagerAdapter extends FragmentStatePagerAdapter {
    public PagerAdapter(FragmentManager fm) {
      super(fm);
    }

    @Override public CharSequence getPageTitle(int position) {
      switch (position) {
        case 0:
          return "СОБЫТИЯ";
        case 1:
          return "КАРТА";
        case 2:
          return "СПИСОК";
        default:
          return null;
      }
    }

    @Override public Fragment getItem(int position) {
      switch (position) {
        case 0:
          return EventsFragment.newInstance();
        case 1:
          return MapFragment.newInstance(false);
        case 2:
          return ApieryFragment.newInstance();
        default:
          return null;
      }
    }

    @Override public int getCount() {
      return 3;
    }

    //        public int getItemPosition(Object object) {
    //            return POSITION_NONE;
    //        }
  }
}
