/*
 * Copyright (c) 2017 Oleg Filimonov
 */

package ru.olegfilimonov.apiery.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import ru.olegfilimonov.apiery.ApieryApplication;
import ru.olegfilimonov.apiery.R;
import ru.olegfilimonov.apiery.activity.HiveActivity;
import ru.olegfilimonov.apiery.model.Apiery;
import ru.olegfilimonov.apiery.model.Constant;
import ru.olegfilimonov.apiery.model.Hive;
import ru.olegfilimonov.apiery.model.MapState;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends Fragment
    implements OnMapReadyCallback, GoogleMap.OnCameraMoveListener, GoogleMap.OnCameraIdleListener, GoogleMap.OnCameraMoveCanceledListener, GoogleMap.OnCameraMoveStartedListener {
  private static final String TAG = "MAIN";
  @BindView(R.id.distance_seekbar) SeekBar distanceSeekBar;
  @BindView(R.id.distance_text) CheckBox distanceCheckBox;
  @BindView(R.id.distance_value) TextInputLayout distanceValue;
  private MapState state;
  private GoogleMap map;
  private List<Apiery> apieries;
  private ArrayList<Marker> apieryMarkers;
  private Hashtable<String, ArrayList<Marker>> apieryIdToHiveListMap;
  private Unbinder unbinder;
  private int radius = Constant.DEFAULT_DISTANCE;
  private ArrayList<Circle> circles = new ArrayList<>();
  private boolean addingMode;
  private Marker addMarker;
  private Circle addCircle;
  private ArrayList<Polygon> apieryIdToPolygonMap;

  public MapFragment() {
    // Required empty public constructor
  }

  public static MapFragment newInstance(boolean addingMode) {
    MapFragment fragment = new MapFragment();
    fragment.setAddingMode(addingMode);
    return fragment;
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view = inflater.inflate(R.layout.fragment_map, container, false);
    unbinder = ButterKnife.bind(this, view);
    this.apieries = ApieryApplication.getInstance().getApieries();

    // Obtain the SupportMapFragment and get notified when the map is ready to be used.
    SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
    mapFragment.getMapAsync(this);

    apieryIdToHiveListMap = new Hashtable<>();
    apieryIdToPolygonMap = new ArrayList<>();
    apieryMarkers = new ArrayList<>();

    return view;
  }

  @Override public void onMapReady(GoogleMap googleMap) {
    map = googleMap;

    LatLng test = new LatLng(56.097338, 92.880463);

    map.moveCamera(CameraUpdateFactory.newLatLngZoom(test, 17));
    map.setOnCameraMoveListener(this);
    map.setOnCameraIdleListener(this);
    map.setOnCameraMoveCanceledListener(this);
    map.setOnCameraMoveStartedListener(this);
    map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

      @Override public View getInfoWindow(Marker arg0) {
        return null;
      }

      @Override public View getInfoContents(Marker marker) {

        Context context = getActivity(); //or getActivity(), YourActivity.this, etc.

        LinearLayout info = new LinearLayout(context);
        info.setOrientation(LinearLayout.VERTICAL);

        TextView title = new TextView(context);
        title.setTextColor(Color.BLACK);
        title.setGravity(Gravity.CENTER);
        title.setTypeface(null, Typeface.BOLD);
        title.setText(marker.getTitle());

        TextView snippet = new TextView(context);
        snippet.setTextColor(Color.GRAY);
        snippet.setText(marker.getSnippet());

        info.addView(title);
        info.addView(snippet);

        return info;
      }
    });
    map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
      @Override public void onInfoWindowClick(Marker marker) {
        if (marker.getTag() instanceof Hive) {
          Hive hive = (Hive) marker.getTag();
          Context context = getActivity();
          Intent intent = new Intent(context, HiveActivity.class);
          intent.putExtra("hive_id", hive.getId());
          context.startActivity(intent);
        }
      }
    });

    if (addingMode) setupAddingMode();
    setupMarkers();
    updateMarkers();
    setupDistance();
  }

  private void setupAddingMode() {
    LatLng target = map.getCameraPosition().target;

    MarkerOptions markerOptions = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.hive)).position(target).anchor(.5f, .5f).alpha(.5f);

    CircleOptions circleOptions = new CircleOptions();
    circleOptions.center(target);

    int color11 = getResources().getColor(R.color.colorPrimaryLight);
    int color12 = getResources().getColor(R.color.colorPrimary);

    int color21 = Color.argb(Color.alpha(color11) / 2, Color.red(color11), Color.green(color11), Color.blue(color11));
    int color22 = Color.argb(Color.alpha(color12) / 2, Color.red(color12), Color.green(color12), Color.blue(color12));

    circleOptions.fillColor(color21);
    circleOptions.strokeColor(color22);

    addCircle = map.addCircle(circleOptions);
    addMarker = map.addMarker(markerOptions);

    circles.add(addCircle);
  }

  private void setupDistance() {

    // TODO: 30/11/2016 Replace this with server params
    boolean distanceEnabled = Constant.DEFAULT_DISTANCE_ENABLED;
    int defaultDistance = Constant.DEFAULT_DISTANCE;
    int defaultMax = Constant.DEFAULT_MAX_DISTANCE;

    setupCircles(defaultDistance);
    distanceCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        MapFragment.this.setDistanceEnabled(isChecked);
      }
    });
    distanceCheckBox.setChecked(distanceEnabled);
    setDistanceEnabled(distanceEnabled);
    distanceSeekBar.setProgress(defaultDistance);
    distanceSeekBar.setMax(defaultMax);
    distanceCheckBox.setChecked(distanceEnabled);
    distanceValue.getEditText().setText(String.valueOf(distanceSeekBar.getProgress()));
    distanceValue.getEditText().addTextChangedListener(new TextWatcher() {
      @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
        try {
          int progress = Integer.valueOf(s.toString());
          updateCircles(progress);
        } catch (Exception e) {
          // ignored
        }
      }

      @Override public void afterTextChanged(Editable s) {

      }
    });
    distanceSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
      @Override public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        try {
          distanceValue.getEditText().setText(String.valueOf(progress));
          updateCircles(progress);
        } catch (Exception e) {
          // ignored
        }
      }

      @Override public void onStartTrackingTouch(SeekBar seekBar) {

      }

      @Override public void onStopTrackingTouch(SeekBar seekBar) {

      }
    });
  }

  private void setupCircles(int distance) {
    for (Apiery apiery : apieries) {
      CircleOptions circleOptions = new CircleOptions();
      circleOptions.center(new LatLng(apiery.getLocation().latitude, apiery.getLocation().longitude));
      circleOptions.fillColor(getResources().getColor(R.color.colorPrimaryLight));
      circleOptions.strokeColor(getResources().getColor(R.color.colorPrimary));
      Circle circle = map.addCircle(circleOptions);
      circles.add(circle);
    }
    updateCircles(distance);
  }

  private void updateCircles(int distance) {
    for (Circle circle : circles) {
      circle.setRadius(distance);
    }
  }

  private void setDistanceEnabled(boolean checked) {
    distanceSeekBar.setVisibility(checked ? View.VISIBLE : View.GONE);
    distanceValue.setVisibility(checked ? View.VISIBLE : View.GONE);
    for (Circle circle : circles) {
      circle.setVisible(checked);
    }
    if (addingMode) {
      addCircle.setCenter(map.getCameraPosition().target);
    }
  }

  private void setupMarkers() {
    for (Apiery apiery : apieries) {

      // Add apiery markers for each apiery
      String snippet = "Ульев: " + apiery.getHives().size();
      String apieryId = String.valueOf(apiery.getId());
      MarkerOptions apieryOptions = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_apiery))
          .position(new LatLng(apiery.getLocation().latitude, apiery.getLocation().longitude))
          .anchor(.5f, .5f).title(apiery.getTitle()).snippet(snippet);

      Marker apieryMarker = map.addMarker(apieryOptions);
      apieryMarkers.add(apieryMarker);

      // Add hives markers for all hives
      List<Hive> hives = apiery.getHives();

      PolygonOptions polygonOptions = new PolygonOptions().strokeColor(getResources().getColor(R.color.colorPrimaryLight));

      for (Hive hive : hives) {
        String hiveSnippet = apiery.getTitle();

        LatLng latLng = new LatLng(hive.getLocation().latitude, hive.getLocation().longitude);
        polygonOptions.add(latLng);
        MarkerOptions hiveOptions = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.hive))
            .position(latLng)
            .anchor(.5f, .5f)
            .title(hive.getName() + (hive.getNote() == null ? "" : ("(" + hive.getNote() + ")")))
            .snippet(hiveSnippet);

        Marker hiveMarker = map.addMarker(hiveOptions);
        hiveMarker.setTag(hive);
        if (apieryIdToHiveListMap.containsKey(apieryId)) {
          ArrayList<Marker> markers = apieryIdToHiveListMap.get(apieryId);
          markers.add(hiveMarker);
        } else {
          ArrayList<Marker> markers = new ArrayList<>();
          markers.add(hiveMarker);
          apieryIdToHiveListMap.put(apieryId, markers);
        }
      }

      Polygon polygon = map.addPolygon(polygonOptions);
      apieryIdToPolygonMap.add(polygon);
    }
  }

  private void updateMarkers() {
    state = map.getCameraPosition().zoom > Constant.APIERIES_ZOOM_LIMIT ? MapState.HIVES : MapState.APIERIES;

    switch (state) {
      case APIERIES:
        showApieries();
        hideHives();
        break;
      case HIVES:
        showHives();
        hideApieries();
        break;
    }
  }

  private void showHives() {
    for (ArrayList<Marker> list : apieryIdToHiveListMap.values()) {
      for (Marker marker : list) {
        marker.setVisible(true);
      }
    }
  }

  private void hideHives() {
    for (ArrayList<Marker> list : apieryIdToHiveListMap.values()) {
      for (Marker marker : list) {
        marker.setVisible(false);
      }
    }
  }

  private void showApieries() {
    for (Marker apiery : apieryMarkers) {
      apiery.setVisible(true);
    }
    for (Polygon polygon : apieryIdToPolygonMap) {
      polygon.setVisible(false);
    }
  }

  private void hideApieries() {
    for (Marker apiery : apieryMarkers) {
      apiery.setVisible(false);
    }
    for (Polygon polygon : apieryIdToPolygonMap) {
      polygon.setVisible(true);
    }
  }

  @Override public void onCameraMoveStarted(int i) {
    // Called on the start of the move

  }

  @Override public void onCameraMove() {
    // Called while the move is happening multiple times
    float zoom = map.getCameraPosition().zoom;
    float limit = Constant.APIERIES_ZOOM_LIMIT;

    if (zoom > limit && state == MapState.APIERIES || zoom <= limit && state == MapState.HIVES) {
      updateMarkers();
    }

    if (addingMode) {
      LatLng target = map.getCameraPosition().target;
      if (addCircle.isVisible()) {
        addCircle.setCenter(target);
      }
      addMarker.setPosition(target);
      ((MapFragmentListener) getActivity()).onCameraUpdate(target);
    }
  }

  @Override public void onCameraIdle() {
    // Called after move is finished

  }

  @Override public void onDestroyView() {
    super.onDestroyView();
    if (unbinder != null) {
      unbinder.unbind();
    }
  }

  @Override public void onCameraMoveCanceled() {
    Log.d(TAG, "onCameraMoveCanceled: ");
  }

  public boolean isAddingMode() {
    return addingMode;
  }

  public void setAddingMode(boolean addingMode) {
    this.addingMode = addingMode;
  }

  public LatLng getLocation() {
    return map.getCameraPosition().target;
  }

  public interface MapFragmentListener {
    void onCameraUpdate(LatLng latLng);
  }
}
