/*
 * Copyright (c) 2017 Oleg Filimonov
 */

package ru.olegfilimonov.apiery.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;
import ru.olegfilimonov.apiery.ApieryApplication;
import ru.olegfilimonov.apiery.R;
import ru.olegfilimonov.apiery.activity.HiveActivity;
import ru.olegfilimonov.apiery.adapter.ApieryListAdapter;
import ru.olegfilimonov.apiery.model.Apiery;
import ru.olegfilimonov.apiery.model.Hive;

import static android.app.Activity.RESULT_OK;

public class ApieryFragment extends Fragment {
  private static final int REQUEST_HIVE = 512;
  private List<Apiery> apieries;
  private ApieryListAdapter adapter;
  private ApieryListAdapter.Listener listener;

  public ApieryFragment() {
  }

  public static ApieryFragment newInstance() {
    ApieryFragment fragment = new ApieryFragment();
    return fragment;
  }

  public static ApieryFragment newInstance(ApieryListAdapter.Listener listener) {
    ApieryFragment fragment = new ApieryFragment();
    fragment.setListener(listener);
    return fragment;
  }

  public void setListener(ApieryListAdapter.Listener listener) {
    this.listener = listener;
    adapter.setListener(listener);
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_apiery_list, container, false);
    this.apieries = ApieryApplication.getInstance().getApieries();

    // Set the adapter
    if (view instanceof RecyclerView) {
      Context context = view.getContext();
      RecyclerView recyclerView = (RecyclerView) view;
      recyclerView.setLayoutManager(new LinearLayoutManager(context));

      ApieryListAdapter.Listener defaultListener;
      defaultListener = new ApieryListAdapter.Listener() {
        @Override public void onClick(Apiery apiery, Hive hive) {

          Intent intent = new Intent(ApieryFragment.this.getContext(), HiveActivity.class);
          intent.putExtra("hive_id", hive.getId());
          ApieryFragment.this.startActivityForResult(intent, REQUEST_HIVE);
        }
      };

      adapter = new ApieryListAdapter(listener == null ? defaultListener : listener, apieries, context);
      recyclerView.setAdapter(adapter);
    }

    return view;
  }

  @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == REQUEST_HIVE) {
      if (resultCode == RESULT_OK) {
        adapter.updateHive((Hive) data.getParcelableExtra("hive"));
        adapter.notifyDataSetChanged();
      }
    }
    super.onActivityResult(requestCode, resultCode, data);
  }

  @Override public void onDetach() {
    super.onDetach();
  }

  public void setApieries(List<Apiery> apieries) {
    this.apieries = apieries;
  }
}
