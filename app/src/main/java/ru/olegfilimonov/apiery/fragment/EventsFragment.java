/*
 * Copyright (c) 2017 Oleg Filimonov
 */

package ru.olegfilimonov.apiery.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.RecyclerViewSwipeManager;
import java.util.List;
import ru.olegfilimonov.apiery.ApieryApplication;
import ru.olegfilimonov.apiery.R;
import ru.olegfilimonov.apiery.adapter.EventAdapter;
import ru.olegfilimonov.apiery.model.Event;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventsFragment extends Fragment {
  @BindView(R.id.events_recyclerview) RecyclerView recyclerView;
  private List<Event> events;
  private Unbinder unbinder;

  public EventsFragment() {
    // Required empty public constructor
  }

  public static EventsFragment newInstance() {
    EventsFragment fragment = new EventsFragment();
    fragment.setEvents(ApieryApplication.getInstance().getEvents());
    return fragment;
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view = inflater.inflate(R.layout.fragment_events, container, false);
    unbinder = ButterKnife.bind(this, view);
    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    // Setup swiping feature and RecyclerView
    RecyclerViewSwipeManager swipeManager = new RecyclerViewSwipeManager();
    recyclerView.setAdapter(swipeManager.createWrappedAdapter(new EventAdapter(getContext(), events)));
    swipeManager.attachRecyclerView(recyclerView);

    return view;
  }

  @Override public void onDestroyView() {
    super.onDestroyView();
    if (unbinder != null) {
      unbinder.unbind();
    }
  }

  public List<Event> getEvents() {
    return events;
  }

  public void setEvents(List<Event> events) {
    this.events = events;
  }
}
