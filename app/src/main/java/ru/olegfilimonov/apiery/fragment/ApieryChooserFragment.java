/*
 * Copyright (c) 2017 Oleg Filimonov
 */

package ru.olegfilimonov.apiery.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.olegfilimonov.apiery.ApieryApplication;
import ru.olegfilimonov.apiery.R;
import ru.olegfilimonov.apiery.adapter.ApieryChooserAdapter;
import ru.olegfilimonov.apiery.model.Apiery;

/**
 * A fragment representing a list of Items.
 * <p />
 * Activities containing this fragment MUST implement the {@link ApieryChooserListener}
 * interface.
 */
public class ApieryChooserFragment extends Fragment {

  private Apiery selectedApiery = null;

  public ApieryChooserFragment() {
  }

  public static ApieryChooserFragment newInstance() {
    return new ApieryChooserFragment();
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_apiery_chooser, container, false);

    Context context = view.getContext();
    RecyclerView recyclerView = (RecyclerView) view;
    recyclerView.setLayoutManager(new LinearLayoutManager(context));
    recyclerView.setAdapter(new ApieryChooserAdapter(ApieryApplication.getInstance().getApieries(), new ApieryChooserListener() {
      @Override public void onItemSelected(Apiery item) {
        selectedApiery = item;
      }
    }, context));

    return view;
  }

  public Apiery getSelectedApiery() {
    return selectedApiery;
  }

  public interface ApieryChooserListener {
    void onItemSelected(Apiery item);
  }
}
