/*
 * Copyright (c) 2017 Oleg Filimonov
 */

package ru.olegfilimonov.apiery.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.google.android.gms.maps.model.LatLng;
import ru.olegfilimonov.apiery.ApieryApplication;
import ru.olegfilimonov.apiery.R;
import ru.olegfilimonov.apiery.model.Hive;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HiveFragmentListener} interface
 * to handle interaction events.
 */
public class NewHiveFragment extends Fragment {
  @BindView(R.id.hivedetail_added) TextView added;
  @BindView(R.id.hivedetail_location) TextView location;
  @BindView(R.id.hivedetail_name) TextInputLayout name;
  @BindView(R.id.hivedetail_note) TextInputLayout note;
  private Unbinder unbinder;
  private HiveFragmentListener listener;
  private LatLng latLng;
  private Hive hive;
  private long apieryId;

  public NewHiveFragment() {
  }

  public static NewHiveFragment newInstance() {
    NewHiveFragment fragment = new NewHiveFragment();
    return fragment;
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

    // Inflate the layout for this fragment
    View view = inflater.inflate(R.layout.fragment_new_hive, container, false);

    unbinder = ButterKnife.bind(this, view);

    return view;
  }

  public void onHiveSubmit(Hive hive) {
    if (listener != null) {
      listener.onHiveSubmit(hive);
    }
  }

  @Override public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof HiveFragmentListener) {
      listener = (HiveFragmentListener) context;
    } else {

      throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
    }
  }

  @Override public void onDetach() {
    super.onDetach();
    listener = null;
  }

  public LatLng getLatLng() {
    return latLng;
  }

  public void setLatLng(LatLng latLng) {
    this.latLng = latLng;
  }

  public Hive getHive() {
    return hive;
  }

  public void setApieryId(long apieryId) {
    this.apieryId = apieryId;
  }

  public void setupView() {
    long id = ApieryApplication.getInstance().getNextUnusedHiveId();

    hive = new Hive();
    hive.setId(id);
    hive.setLocation(new ru.olegfilimonov.apiery.model.LatLng(latLng));
    hive.setCreatedDateTime(System.currentTimeMillis());
    hive.setApieryId(apieryId);

    added.setText("Добавлен 11/23/16");

    location.setText(String.valueOf(hive.getLocation().latitude) + "; " + String.valueOf(hive.getLocation().longitude));
    name.getEditText().setText(hive.getName());
    note.getEditText().setText(hive.getNote());

    note.getEditText().addTextChangedListener(new TextWatcher() {
      @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
        hive.setNote(s.toString());
      }

      @Override public void afterTextChanged(Editable s) {

      }
    });
    name.getEditText().addTextChangedListener(new TextWatcher() {
      @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
        hive.setName(s.toString());
      }

      @Override public void afterTextChanged(Editable s) {

      }
    });
  }

  public interface HiveFragmentListener {
    void onHiveSubmit(Hive hive);
  }
}
