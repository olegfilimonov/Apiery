package ru.olegfilimonov.apiery.firebase;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * @author Oleg Filimonov
 */

public class ApieryInstanceService extends FirebaseInstanceIdService {

  private static final String TAG = "FCM";

  @Override public void onTokenRefresh() {
    // Get updated InstanceID token.
    String refreshedToken = FirebaseInstanceId.getInstance().getToken();
    Log.d(TAG, "Refreshed token: " + refreshedToken);

    Intent sendIntent = new Intent();
    sendIntent.setAction(Intent.ACTION_SEND);
    sendIntent.putExtra(Intent.EXTRA_TEXT, refreshedToken);
    sendIntent.setType("text/plain");
    sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    startActivity(sendIntent);
  }
}
