/*
 * Copyright (c) 2017 Oleg Filimonov
 */

package ru.olegfilimonov.apiery.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import java.util.Map;
import ru.olegfilimonov.apiery.ApieryApplication;
import ru.olegfilimonov.apiery.R;
import ru.olegfilimonov.apiery.activity.HiveActivity;
import ru.olegfilimonov.apiery.activity.MainActivity;
import ru.olegfilimonov.apiery.model.Event;

public class ApieryMessagingService extends FirebaseMessagingService {
  private static final String TAG = "FCM";

  @Override public void onMessageReceived(RemoteMessage remoteMessage) {

    Log.d(TAG, "From: " + remoteMessage.getFrom());

    // Check if message contains a data payload
    Map<String, String> data = remoteMessage.getData();
    if (data.size() > 0) {
      Log.d(TAG, "Message data payload: " + data);

      String title = data.get("notification_title");
      String message = data.get("notification_message");
      String hiveId = data.get("hive_id");

      Intent notificationIntent = null;

      if (hiveId != null) {
        // Has a hive
        notificationIntent = new Intent(getApplicationContext(), HiveActivity.class);
        notificationIntent.putExtra("hive_id", Long.parseLong(hiveId));
      } else {
        notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
      }

      sendNotification(title, message, notificationIntent);

      addEventToDatabase(title, message, hiveId);


      return;
    }

    // Check if message contains a notification payload
    if (remoteMessage.getNotification() != null) {
      Intent intent = new Intent(this, MainActivity.class);
      String title = remoteMessage.getNotification().getTitle();
      String message = remoteMessage.getNotification().getBody();

      sendNotification(title, message, intent);
    }
  }

  private void addEventToDatabase(String title, String message, String hiveId) {
    long createdDateTime = System.currentTimeMillis();
    Event event = new Event();
    event.setHiveId(hiveId == null ? null : Long.valueOf(hiveId));
    event.setTitle(hiveId == null ? null
        : ApieryApplication.getInstance().getHiveById(Long.valueOf(hiveId)).getName());
    event.setText(message);
    event.setShownDateTime(createdDateTime);
    ApieryApplication.getInstance().addEvent(event);
  }

  /**
   * Create and show a simple notification containing the received FCM message.
   */
  private void sendNotification(String title, String message, Intent intent) {

    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);

    Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this).setSmallIcon(R.drawable.ic_apiery)
        .setContentTitle(title)
        .setColor(getResources().getColor(R.color.colorPrimary))
        .setContentText(message)
        .setAutoCancel(true)
        .setSound(defaultSoundUri)
        .setContentIntent(pendingIntent);

    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

    notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
  }
}
